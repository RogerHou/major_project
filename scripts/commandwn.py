import pygame
import time
import cv2
import random

from cardclassifier import Cardclassifier

class Commandwn():
    def __init__(self):
        pygame.init()
        self.screen = pygame.display.set_mode((200, 320))
        self.screen.fill((255,255,255))
        pygame.display.set_caption('Instruction Window')
        self.select_flag = 1

    # pygame button
    def button(self, position, text):
        font = pygame.font.SysFont("Arial", 50)
        text_render = font.render(text, 1, (255, 0, 0))
        #x, y, w , h = text_render.get_rect()
        w = 158
        h = 50
        x, y = position
        x = 20
        pygame.draw.line(self.screen, (150, 150, 150), (x, y), (x + w , y), 5)
        pygame.draw.line(self.screen, (150, 150, 150), (x, y - 2), (x, y + h), 5)
        pygame.draw.line(self.screen, (50, 50, 50), (x, y + h), (x + w , y + h), 5)
        pygame.draw.line(self.screen, (50, 50, 50), (x + w , y+h), [x + w , y], 5)
        pygame.draw.rect(self.screen, (100, 100, 100), (x, y, w , h))
        x, y = position
        return self.screen.blit(text_render, (x, y))

    # pygame message
    def message_display(self,txt):
        self.screen.fill((0,128,0))
        font = pygame.font.Font('freesansbold.ttf', 32)
        msg = ''
        for i in range(len(txt)):
            msg = msg + txt[i] + ' '
            text = font.render(txt[i], True, (255,0,0), (240, 230, 140))
            textRect = text.get_rect()
            textRect.center = (100, 160+60*i-30*(len(txt)-1))
            self.screen.blit(text, textRect)
        pygame.display.update()

        # also display in the terminal
        print (msg)

    def menu(self,classifier,robot):
        """ This is the menu that waits you to click
        the s key to start """
        self.screen.fill((0,128,0))
        b1 = self.button((60, 140), "Hit!")
        b2 = self.button((40, 240), "Pass")

        if self.select_flag == 2:
            b3 = self.button((20, 40), "---------")
        else:
            b3 = self.button((20, 40), "Double")
        while self.select_flag > 0 :
            pygame.display.update()

            for event in pygame.event.get():
                #if (event.type == pygame.QUIT):
                #    pygame.quit()
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_RETURN:
                        # read image
                        while True:
                            img = robot.Leftdecision()
                            label = classifier.action_classify(img)
                            if label == 'red':
                                return 1
                            elif label == 'green':
                                return 0
                            elif (label == 'yellow' and self.select_flag != 2):
                                return 2
                    elif event.key == pygame.K_ESCAPE:
                        pygame.quit()
                if event.type == pygame.MOUSEBUTTONDOWN:
                    if b1.collidepoint(pygame.mouse.get_pos()):
                        return 1
                    elif b2.collidepoint(pygame.mouse.get_pos()):
                        return 0
                    elif b3.collidepoint(pygame.mouse.get_pos()):
                        if self.select_flag != 2:
                            return 2

        #pygame.quit()

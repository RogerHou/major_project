import turtle
import time
import cv2
import random

from card import Card
from table import Table
from deck import Deck
from player import Player
from cardclassifier import Cardclassifier
from arm import ik_test,ArmPlanner

class Dealer():
    def __init__(self, player_num,table_w,table_h,wn):
        self.cards = []
        self.number = 0
        self.point = 0
        self.ace_flag = 0
        self.deck = Deck()
        self.table = Table(player_num,table_w,table_h)
        self.players = []
        self.player_num = player_num
        self.val_table = []
        self.coins = 0  # how much money earn
        if player_num == 3:
            self.x = round(-table_h/4)
        else:
            self.x = 0

        self.y = 150
        self.pen = turtle.Turtle()
        self.pen.speed(0)
        self.pen.hideturtle()
        self.wn = wn
        # another pen to draw bets
        self.penx = turtle.Turtle()
        self.penx.speed(0)
        self.penx.hideturtle()

        # set a wating flag for the robot
        self.wait_flag = 0

        # robot setting
        self.poker_classifier = Cardclassifier()

        # initalize the player
        for i in range(self.player_num):
            name = raw_input("Enter Player"+str(i+1)+"'s name: ")
            player = Player(name,i+1,self.player_num,table_h)
            self.players.append(player)

        # tell how many cards has been drawn 
        self.cards_drawn = 0 
        self.robot = ArmPlanner()

    def player_draw(self, i):
        # display the robot is dealing cards to player
        txt = ["card","recognition"]
        self.wn.message_display(txt)

        card = self.getcard()

        # while self.wait_flag ~= 1
        #time.sleep(1)

        self.players[i].point = self.players[i].point + card.value

        if card.value == 1:
            self.players[i].ace_flag = self.players[i].ace_flag + 1
            self.players[i].point = self.players[i].point + 10

        while (self.players[i].point > 21 and self.players[i].ace_flag > 0):
            self.players[i].point = self.players[i].point - 10
            self.players[i].ace_flag = self.players[i].ace_flag - 1

        self.players[i].number = self.players[i].number + 1
        self.players[i].cards.append(card)
        self.table.draw_cards(self.players[i].x,self.players[i].y,self.players[i].cards,i+1,self.players[i].point,self.players[i].pen)

        # display the robot is dealing cards to player
        txt = ["dealing","to",self.players[i].name]
        self.wn.message_display(txt)

        # dealing function
        self.robot.RightOverCard()
        goal= self.robot.limb_right.endpoint_pose()
        joint = ik_test('right',goal,self.cards_drawn)
        self.robot.limb_right.move_to_joint_positions(joint)
        self.robot._gripperR.set_vacuum_threshold(6.0)
        attached = self.robot._gripperR.command_suction(1, 2)
       
        while not attached:
	 		attached = self.robot._gripperR.command_suction(1, 2)
        
        raw_input("Press <Enter> to grab the card")
        self.robot.RightOverCard()
        
        if i == 0:
            self.robot.right_deal_player1()
        elif i ==1:
            self.robot.right_deal_player2()
        elif i ==2:
            self.robot.right_deal_player3()   
             
        self.robot._gripperR.set_blow_off(1)
	self.robot._gripperR.stop()


    def banker_draw(self):
        # display the robot is dealing cards to player
        txt = ["card","recognition"]
        self.wn.message_display(txt)

        card = self.getcard()
        # machine learning

        # while self.wait_flag ~= 1
        #time.sleep(1)
        self.wait_flag = 1;

        self.point = self.point + card.value
        if card.value == 1:
            self.ace_flag = 1
            self.point = self.point + 10

        while (self.point > 21 and self.ace_flag > 0):
            self.point = self.point - 10
            self.ace_flag = self.ace_flag - 1

        if self.point > 21:
            self.point = 0

        self.number = self.number + 1
        self.cards.append(card)
        self.table.draw_cards(self.x,self.y,self.cards,0,self.point,self.pen)

        # display the robot is dealing cards to player
        txt = ["dealing","to","banker"]
        self.wn.message_display(txt)

        # dealing function
        self.robot.RightOverCard()
        goal= self.robot.limb_right.endpoint_pose()
        joint = ik_test('right',goal,self.cards_drawn)
        self.robot.limb_right.move_to_joint_positions(joint)
        self.robot._gripperR.set_vacuum_threshold(6.0)
        attached = self.robot._gripperR.command_suction(1, 2)
        
        while not attached:
	 		attached = self.robot._gripperR.command_suction(1, 2)
        raw_input("Press <Enter> to grab the card")
        self.robot.RightOverCard()
        
        self.robot.right_deal_baxter()
        self.robot._gripperR.set_blow_off(1)
	self.robot._gripperR.stop()


    def place_bet(self):
        for i in range(self.player_num):
            self.players[i].bet = int(raw_input(self.players[i].name + " Please place your bet (1 - 20): "))
            while (self.players[i].bet > 20 or self.players[i].bet < 1):
                print("The allowable range is 1 - 20!")
                self.players[i].bet = int(raw_input(self.players[i].name + " Please place again: "))

    def value_table(self):
        self.val_table = []
        for card in self.deck.cards:
            self.val_table.append(card.value)
        self.val_table.sort()

    def decision(self):
        current_win = 0
        next_win = 0
        point_bj = 21 - self.point
        bj_num = 0

        # if bust
        if (self.point <= 0 or self.point >21):
            return

        # load deck into memory
        self.value_table()

        # count rate for each player
        for i in range(self.player_num):
            # calculate if the player is not blackjack
            if (self.players[i].point > 0 and self.players[i].point < 21):
                # calculate the difference
                diff = self.players[i].point - self.point

                # count current income ratio
                if  diff < 0:
                    current_win = current_win + self.players[i].bet
                elif diff > 0:
                    current_win = current_win - self.players[i].bet

                # count rato of the next card to win
                loss_rate = sum(j > point_bj for j in self.val_table)/len(self.val_table)
                win_rate = sum((j <= point_bj and j > diff) for j in self.val_table)/len(self.val_table)

                # count the next income ratio
                next_win = next_win + self.players[i].bet*win_rate - self.players[i].bet*loss_rate
            elif self.players[i].point == 21:
                current_win = current_win - self.players[i].bet
                next_win = next_win - self.players[i].bet
                bj_num = bj_num + 1
            elif self.players[i].point == 0:
                current_win = current_win + self.players[i].bet
                next_win = next_win + self.players[i].bet

        # give up if all players got blackjack
        if bj_num == self.player_num:
            return 0

        if (self.ace_flag > 0 and current_win <= 0):
            return 1

        if (next_win > current_win or current_win == -self.player_num):
            return 1

    def judge(self):
        # judge the result
        for i in range(self.player_num):
            if (self.point < self.players[i].point and self.players[i].point < 22 and self.players[i].point > 0)or(self.players[i].point == 21):
                print("Player "+self.players[i].name+" win")
                self.coins = self.coins - self.players[i].bet
            elif (self.point == self.players[i].point and self.players[i].point !=0 and self.players[i].point !=21):
                print("Player "+self.players[i].name+" draw")
            else:
                print("Player "+self.players[i].name+" loss")
                self.coins = self.coins + self.players[i].bet

            # show statistics
            self.table.result(self.players[i].x,self.players[i].y,self.players[i].point,self.point,self.players[i].pen)
        self.table.show_bet(self.x,self.y,self.coins,self.pen)

    def getcard(self):
        # moveit
        image = self.robot.RightHandLook()

        while True:
            # recogniztion
            rank,suit = self.poker_classifier.detect_card(image)
            #print rank,suit
            if self.deck.remove(rank,suit) == 0:
                self.cards_drawn = self.cards_drawn + 1
                return Card(rank,suit)
            else:
                time.sleep(3)
                image = self.robot.RightHandLook()

    def refresh(self):
        self.cards = []
        self.number = 0
        self.point = 0
        self.ace_flag = 0
        self.val_table = []
        self.pen.clear()
        self.table.pen.clear()
        self.penx.clear()
        self.cards_drawn = 0

    def game(self):
        names = []

        # ask for bet placing
        self.place_bet()

        # get the name of players
        for i in range(self.player_num):
            names.append(self.players[i].name)

        self.deck.reset_deck()

        #for card in self.deck.cards:
        #    print card.rank,card.suit

        self.table.frame(names)

        # show bet
        for i in range(self.player_num):
            self.table.show_bet(self.players[i].x,self.players[i].y,self.players[i].bet,self.players[i].bet_pen)

        # start the game by dealling cards
        for i in range(self.player_num):
            self.player_draw(i)

        # one card for the dealer
        self.banker_draw()

        # second run for the players
        for i in range(self.player_num):
            self.player_draw(i)

        # ask the player
        for i in range(self.player_num):
            # reset the flag for the next player
            self.wn.select_flag = 1

            # check if start black jack
            if self.players[i].point == 21:
                print("Player "+self.players[i].name+" receives blackjack")
            else:
                while True:
                    print("Player "+self.players[i].name+" please select your response: ")
                    choice = self.wn.menu(self.poker_classifier)

                    if choice == 0:
                        break
                    elif choice == 1:
                        self.player_draw(i)
                        # update the flag
                        self.wn.select_flag = 2
                    elif (choice == 2 and self.wn.select_flag != 2):
                        self.players[i].bet = self.players[i].bet*2
                        self.players[i].bet_pen.clear()
                        self.table.show_bet(self.players[i].x,self.players[i].y,self.players[i].bet,self.players[i].bet_pen)
                        self.player_draw(i)
                        break

                    if self.players[i].point > 21:
                        print("Player "+self.players[i].name+" bust")
                        self.players[i].point = 0
                        break
                    elif self.players[i].point == 21:
                        print("Player "+self.players[i].name+" receives blackjack")
                        break


        # banker second card
        self.banker_draw()

        # add delay
        time.sleep(0.5)

        # choose hit or stand
        while self.decision() == 1:
            # show the choice
            self.penx.penup()
            if self.player_num == 3:
                self.penx.goto(-80,250)
            else:
                self.penx.goto(320,250)

            self.penx.pendown()
            self.penx.color("#FF796C")
            self.penx.write("Hit",False,font=("Courier New", 25, "bold"))

            # draw new card
            self.banker_draw()

            # clear the note
            self.penx.clear()

            if self.point == 0:
                break

            # add delay
            time.sleep(0.5)
        # show results
        self.judge()

import turtle

class Player():
    def __init__(self, name, ID, player_num,table_h):
        self.cards = []
        self.name = name
        self.number = 0
        self.point = 0
        self.ace_flag = 0
        self.bet = 1 # default bet
        if ID == 1:
            if player_num == 1:
                self.x = 0
                self.y = -150
            elif player_num == 2:
                self.x = round(-table_h/4)
                self.y = -150
            elif player_num == 3:
                self.x = round(table_h/4)
                self.y = 150
        elif ID == 2:
            self.y = -150
            if player_num == 2:
                self.x = round(table_h/4)
            elif player_num == 3:
                self.x = round(-table_h/4)
        elif ID == 3:
            self.x = round(table_h/4)
            self.y = -150

        self.pen = turtle.Turtle()
        self.pen.speed(0)
        self.pen.hideturtle()

        self.bet_pen = turtle.Turtle()
        self.bet_pen.speed(0)
        self.bet_pen.hideturtle()

    def refresh(self):
        self.cards = []
        self.number = 0
        self.point = 0
        self.ace_flag = 0
        self.bet = 1 # default bet
        self.pen.clear()
        self.bet_pen.clear()

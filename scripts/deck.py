import random
from random import shuffle
from card import Card

ranks = ("A","K","Q","J","10","9","8","7","6","5","4","3","2")
suits = ("spades","hearts","clubs","diamonds")

# deck class
class Deck():
    def __init__(self):
        self.cards = []
        for rank in ranks:
            for suit in suits:
                card = Card(rank,suit)
                self.cards.append(card)

    def shuffle(self):
        random.shuffle(self.cards)

    def draw(self):
        card = self.cards.pop()
        return card

    def remove(self,rank,suit):

        for i, o in enumerate(self.cards):
            if (o.rank == rank and o.suit == suit):
                del self.cards[i]
                return 0
        #for i, o in enumerate(self.cards):
        #    print o.rank,o.suit
        print("Undefined card or The current card should not be in the Desk:")
        print("1. Please wait or 2. Remove this card.")
        return 1

        #cards = [card for card in cards if (card.rank != rank and card.suit != suit)]

    def reset_deck(self):
        self.cards = []

        for rank in ranks:
            for suit in suits:
                card = Card(rank,suit)
                self.cards.append(card)
        self.shuffle()

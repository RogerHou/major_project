#!/usr/bin/env python
import sys
import rospy
import numpy as np
import math
import os
import cv2
import cv_bridge

from cardclassifier import Cardclassifier
from moveit_msgs.msg import OrientationConstraint
#from sensor_msgs.msg import Image
#from arm_kinematics import *
from arm import *
from baxter_interface import Limb
from controller import Controller
from baxter_interface import gripper as robot_gripper
from sensor_msgs.msg import (Image,Range)

#from gesture_classifier import GestureClassifier
#from card_classifier import CardClassifier

robot = None
root_directory = "/home/roger/image/"
saved_image_path = "/home/roger/Ganlan"

def IR_callback(msg):
    global distance
    distance = msg
    #print(msg.range)

def display_image(path, sleepTime = 0):

    img = cv2.imread(path)
    msg = cv_bridge.CvBridge().cv2_to_imgmsg(img, encoding="bgr8")
    img_pub = rospy.Publisher('/robot/xdisplay', Image, latch=True, queue_size=1)
    img_pub.publish(msg)

def main():
	"""
	Main Script
	"""
	global robot, cards_drawn, gesture_cls, card_cls, numBusted,counter
	# Make sure that you've looked at and understand path_planner.py before starting
	rospy.init_node('moveit_node')
	robot = ArmPlanner()
	
	# limb_right = baxter_interface.Limb('right')
	# limb_left = baxter_interface.Limb('left')


    #     raw_input("Test>>>>>>>>>>>>>>>>>>>")
    #     image = robot.Left_read_image()
	# cv2.imshow('image',image)
	# cv2.waitKey()

	# robot.home_pose()

	raw_input("Take Card Pose")
	image = robot.RightHandLook()
	poker_classifier = Cardclassifier()

	#img = cv2.imread(path)
	rank,suit = poker_classifier.detect_card(image)

	print(rank)
	print(suit)

	raw_input("Card Over Pose")
	robot.RightOverCard()

	raw_input('right current pose')
	goal= robot.limb_right.endpoint_pose()

	# raw_input('Desicion')
	# image = robot.Leftdecision()
	# #img = cv2.imread(path)
	# label =Cardclassifier.action_classify(image)
	# print(label)

	for cards_drawn in range(3):

		#print(goal.get("position").get("Point"))
		joint = ik_test('right',goal,cards_drawn)

		robot.limb_right.move_to_joint_positions(joint)

		raw_input("Set the threshold")
		robot._gripperR.set_vacuum_threshold(6.0)
		print(robot._gripperR.vacuum_sensor())
		attached = robot._gripperR.command_suction(1, 2)
		print(attached)
		while not attached:
	 		attached = robot._gripperR.command_suction(1, 2)


		raw_input("Card Over Pose")
		robot.RightOverCard()



	# 	raw_input("Deal to Player 1")
	# 	robot.right_deal_player1()

	# 	raw_input("Release Card")
	# 	robot._gripperR.set_blow_off(1)
	# 	robot._gripperR.stop()

	# 	raw_input("Take Card Pose")
	# 	image = robot.RightHandLook()

	# 	poker_classifier = Cardclassifier()

	# 	#img = cv2.imread(path)
	# 	rank,suit = poker_classifier.detect_card(image)

	# 	print(rank)
	# 	print(suit)

	# 	raw_input("Card Over Pose")
	# 	robot.RightOverCard()



	#print(Solution)

	#print(goal["position"][0])

	# counter = 0
	# for i in range(52):
	# raw_input("Get the image")
	# cv2_img = cv_bridge.CvBridge().imgmsg_to_cv2(decision,"bgr8")
	# cv2.imwrite(os.path.join(saved_image_path,'test%d.jpg'%(counter)),cv2_img)
	# counter = counter +1



	# while distance.range > 0.05:


        #robot.right_take_card()

	# robot._gripperR.set_vacuum_threshold(6.0)
	# print(robot._gripperR.vacuum_sensor())
	# # make sure card attached
	# attached = robot._gripperR.command_suction(1, 2)
	# print(attached)

	# while not attached:
	#  	attached = robot._gripperR.command_suction(1, 2)
#raw_input('left current pose')



	# Solution = ik_test('right')
	# print(Solution)
	# robot.limb_right.move_to_joint_positions(Solution)



	# # raw_input('shit')
	# # print(distance.range)
	# raw_input('IK')
	# Solution = ik_test('right')
	# print(Solution)
	# robot.limb_right.move_to_joint_positions(Solution)

        #raw_input('Function Test to deal to the baxter')
        #robot.right_deal_baxter()

        #raw_input('Function Test to Left Right interactionArmPlanner
#class Player:n()

        #raw_input('Function Test to deal to player1')
        # robot.left_deal_player1()

        # raw_input('Function Test to deal to player2')
        # robot.left_deal_player2()

        # raw_input('Function Test to deal to player3')
        # robot.left_deal_player3()
	#gesture_cls = GestureClassifier()
	#card_cls = CardClassifier()
	#jointsL = [0.0, 0.0, 0.0, 0.0, 0.2, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
	#jointsL = robot.formatJoints(jointsL, 0)
	# jointsR = [0.0, -0.08015049616701286, -0.41800976469877527, 0.5407282277296084, 1.4239176663546353, 0.934194299822217, -1.8024274257653612, -0.5483981316690354, -0.9828981898375788, 1.193437052974852, 1.9055876337506552, -0.8505923468824619, 0.8379370053824072, -2.455519746207576, 1.584218658688661, -2.6948207491177008, -12.565987119160338]
	# jointsR = robot.formatJoints(jointsR)

	# #
	# jointDict = {}
	# j = robot._group.get_joints()
	# # print(joints)
	# # for index, jo in enumerate(j):
	# # 	print(str(index) + ", " + str(jo))
	# #for index in range(1,8):
	# 	#jointDict[j[index]] = jointsL[index - 1]
	# for inde
#class Player:roup.set_joint_value_target(jointDict)
	# robot._group.set_start_state_to_current_state()ArmPlanner
	# cv2_img = cv_bridge.Cvcard
	# raw_input("Blow off")
	# #robot._gripperR.set_blow_off(1)

	# raw_input('Function Test to right hand take')
    #     robot.right_take_card()
	# raw_input('right current pose')
	# robot._gripperR.set_vacuum_threshold(6.0)
	# print(robot._gripperR.vacuum_sensor())
	# # make sure card attached
	# attached = robot._gripperR.command_suction(1, 2)
	# print(attached)

	# while not attached:
	#  	attached = robot._gripperR.command_suction(1, 2)

    # # raw_input('Function Test to right card look')
    # # robot.right_card_look()

	# # raw_input("Take Picuture")
	# # cv2_img = cv_bridge.CvBridge().imgmsg_to_cv2(data,"bgr8")
	# # cv2.imwrite(os.path.join(saved_image_path,'test%d.jpg'%(counter)),cv2_img)
	# # counter = counter +1
	# raw_input('right current pose')
	# robot._gripperR.set_blow_off(1)
	# robot._gripperR.stop()

	# robot._group.execute(plan)
	# print("Right_Over_Deck finished")

	# #robot.right_over_deck()
	# #robot.right_draw_card(depthOffset)
	# raw_input("Press <Enter> to look at the card")
	# robot.right_card_look()
	# print("BLACKJACK Creating players")

	#robot.home_pose()

	# raw_input("Press <Enter> to Start suction ")
	# print(robot._gripperR.vacuum_sensor())
	#robot._gripperR.set_vacuum_threshold(8.0)
	# print(robot._gripperR.vacuum_sensor())
	# # make sure card attached
	# attached = robot._gripperR.command_suction(1, 2)
	# print(attached)

	# while not attached:
	# 	attached = robot._gripperR.command_suction(1, 2)

	# rospy.sleep(0.5)
	# raw_input("Press <Enter> to Blow off ")
	# print(robot._gripperR.vacuum_sensor())
	# robot._gripperR.set_blow_off(1)
	# print(robot._gripperR.vacuum_sensor())
	# robot._gripperR.stop()
	# print("Dealer Dealt")

    	#counter = 2

	# raw_input("Display image")
    # 	display_image(root_directory+"Razer.png",1)
    	#raw_input("Right Card Look and save the image to the folder")

	# # create instances of baxter_interface's Limb class
    # 	limb_right = baxter_interface.Limb('right')
	# limb_left = baxter_interface.Limb('left')



	#raw_input("Press <Enter> to Blow off ")










	while not rospy.is_shutdown():
		while not rospy.is_shutdown():
			try:
				raw_input("Press <Enter> to shut down ")


				#robot.home_pose()
				robot.shutdown()

				# goal_3 = PoseStamped()
				# goal_3.header.frame_id = "base"

				# #x, y, and z position
				# goal_3.pose.position.x = 0.440
				# goal_3.pose.position.y = -0.012
				# goal_3.pose.position.z = 0.549

				# #Orientation as a quaternion
				# goal_3.pose.orientation.x = -0.314
				# goal_3.pose.orientation.y = -0.389
				# goal_3.pose.orientation.z = 0.432
				# goal_3.pose.orientation.w = 0.749

				# #plan = plannerR.plan_to_pose(goal_3, list())

				# raw_input("Press <Enter> to move the right arm to goal pose 3: ")
				# if not plannerR.execute_plan(plannerR.plan_to_pose(goal_3, list())):
				# 	raise Exception("Execution failed")
				exit(0)
			except Exception as e:
				print e
			else:
				break

if __name__ == '__main__':
	rospy.init_node('moveit_node')
	main()

#!/usr/bin/env python
import argparse
import struct
import sys
import rospy
import moveit_commander
import cv2
import cv_bridge

# Import Required Package
from moveit_msgs.msg import OrientationConstraint, Constraints, CollisionObject
from sensor_msgs.msg import Image
from geometry_msgs.msg import (
    PoseStamped,
    Pose,
    Point,
    Quaternion,
)
import baxter_interface
from std_msgs.msg import Header
from baxter_core_msgs.srv import (
    SolvePositionIK,
    SolvePositionIKRequest,
)

# Inverse Kinematics Function
def ik_test(limb,goal,depthOffset):

    # Request for the IK service
    ns = "ExternalTools/" + limb + "/PositionKinematicsNode/IKService"
    iksvc = rospy.ServiceProxy(ns, SolvePositionIK)
    ikreq = SolvePositionIKRequest()
    hdr = Header(stamp=rospy.Time.now(), frame_id='base')

    # Select the right hand and left hand pose to adjust
    poses = {
        'left': PoseStamped(
            header=hdr,
            pose=Pose(
                position=Point(
                    x=goal["position"][0],
                    y=goal["position"][1],
                    z=goal["position"][2],
                ),
                orientation=Quaternion(
                    x=goal["orientation"][0],
                    y=goal["orientation"][1],
                    z=goal["orientation"][2],
                    w=goal["orientation"][3],
                ),
            ),
        ),
        'right': PoseStamped(
            header=hdr,
            pose=Pose(
                position=Point(
                    x=goal["position"][0],
                    y=goal["position"][1],
                    z=goal["position"][2] - 0.086 - depthOffset*0.001,
                ),
                orientation=Quaternion(
                    x=goal["orientation"][0],
                    y=goal["orientation"][1],
                    z=goal["orientation"][2],
                    w=goal["orientation"][3],
                ),
            ),
        ),
    }

    # Append the desired pose
    ikreq.pose_stamp.append(poses[limb])
    try:
        rospy.wait_for_service(ns, 5.0)
        resp = iksvc(ikreq)
    except (rospy.ServiceException, rospy.ROSException), e:
        rospy.logerr("Service call failed: %s" % (e,))
        return 1

    # Check if result valid, and type of seed ultimately used to get solution
    # convert rospy's string representation of uint8[]'s to int's
    resp_seeds = struct.unpack('<%dB' % len(resp.result_type),
                               resp.result_type)

    # Check if the joint angles are valid
    if (resp_seeds[0] != resp.RESULT_INVALID):
        seed_str = {
                    ikreq.SEED_USER: 'User Provided Seed',
                    ikreq.SEED_CURRENT: 'Current Joint Angles',
                    ikreq.SEED_NS_MAP: 'Nullspace Setpoints',
                   }.get(resp_seeds[0], 'None')

        limb_joints = dict(zip(resp.joints[0].name, resp.joints[0].position))

    else:
        print("INVALID POSE - No Valid Joint Solution Found.")

    return limb_joints

# Arm planner class represents the robot 
class ArmPlanner(object):
	def __init__(self):
		"""
		Constructor.
		Inputs: None
		group_name:
			Baxter: 'left_arm' or 'right_arm'
		"""

		# If the node is shutdown, call this function
		rospy.on_shutdown(self.shutdown)

		# Initialize moveit_commander
		moveit_commander.roscpp_initialize(sys.argv)

		# Initialize the robot
		self._robot = moveit_commander.RobotCommander()

		# Initialize the planning scene
		self._scene = moveit_commander.PlanningSceneInterface()

		# This publishes updates to the planning scene
		self._planning_scene_publisher = rospy.Publisher('/collision_object', CollisionObject, queue_size=10)

		# Instantiate a move group
		self._groupR = moveit_commander.MoveGroupCommander("right_arm")

		# Set the maximum time MoveIt will try to plan before giving up
		self._groupR.set_planning_time(5)

        # Set the Replanning time
		self._groupR.allow_replanning(1)

		# Instantiate a move group
		self._groupL = moveit_commander.MoveGroupCommander("left_arm")

		# Set the maximum time MoveIt will try to plan before giving up
		self._groupL.set_planning_time(5)

		self._groupL.allow_replanning(1)

        # Set the right and left gripper attribute
		self._gripperR = baxter_interface.gripper.Gripper("right")
		self._gripperL = baxter_interface.gripper.Gripper("left")

		self._gripperR.calibrate()
		self._gripperL.calibrate()

        # Set the right and left Limb Attribute
		self.limb_right = baxter_interface.Limb('right')
		self.limb_left = baxter_interface.Limb('left')

		self._group = moveit_commander.MoveGroupCommander("both_arms")

        # Set the subscriber and Publisher
        self.righthandsubs = rospy.Subscriber('/cameras/right_hand_camera/image',Image,self.Right_image_callback)
        self.lefthandsubs = rospy.Subscriber('/cameras/left_hand_camera/image',Image,self.Left_image_callback)

	     rospy.sleep(0.5)

    # The left hand camera callback function
    def Right_image_callback(self,msg):
    	global decision_callback
    	decision_callback = msg

    # The Right hand camera callback function
    def Left_image_callback(self,msg):
    	global card_callback
    	card_callback = msg

    # Return the transformed image
    def Left_read_image(self):
    	print("Get the image")
	    	cv2_img = cv_bridge.CvBridge().imgmsg_to_cv2(card_callback,"bgr8")
    	return cv2_img

    # Return the transformed image
    def Right_read_image(self):
    	print("Get the image")
	    	cv2_img = cv_bridge.CvBridge().imgmsg_to_cv2(decision_callback,"bgr8")
    	return cv2_img

	def shutdown(self):
		"""
		Code to run on shutdown. This is good practice for safety
		Currently deletes the object's MoveGroup, so that further commands will do nothing
		"""
		self._groupR = None
		self._groupL = None
		self._group = None
		rospy.loginfo("Stopping Path Planner")

    # Transform the messages format into the input format of the move joint function
	def formatJoints(self, joints, right=1):

        # Check if it's the right or left arm
		if right > 0:
			j = []
			j.extend(joints[11:13])
			j.extend(joints[9:11])
			j.extend(joints[13:-1])
			Limb_state= {'right_s0': j[0], 'right_s1':  j[1],
			'right_w0': j[4], 'right_w1': j[5], 'right_w2': j[6],
			'right_e0': j[2], 'right_e1': j[3]}
			return Limb_state

		else:
			j = []
			j.extend(joints[4:6])
			j.extend(joints[2:4])
			j.extend(joints[6:9])
			Limb_state= {'left_s0': j[0], 'left_s1':  j[1],
			'left_w0': j[4], 'left_w1': j[5], 'left_w2': j[6],
			'left_e0': j[2], 'left_e1': j[3]}
			return Limb_state

	#======================RIGHT ARM==============================

    # The position that the right arm reads the card
	def right_card_look(self):

		# store the home position of the arms
		card_look_position = {'right_s0': 1.2298690966871304, 'right_s1':  0.5365097805629234,
		'right_w0': -0.39346607209260864, 'right_w1': 1.7103885784922364, 'right_w2': 1.271670073157008,
		'right_e0': 3.0541557486798587, 'right_e1': 1.6221846831888251}
		Left_idle = {'left_s0': 0.7040971816394049, 'left_s1': 0.8935438089432535,
		'left_w0': 0.8295001110490375, 'left_w1': 1.5665778796279788, 'left_w2': -0.11888351106111957,
		 'left_e0': -1.7452866414166295, 'left_e1': 1.6083788560978562}

		# move both arms to home position
		self.limb_left.move_to_joint_positions(Left_idle)
		self.limb_right.move_to_joint_positions(card_look_position)

    # The position that the right arm takes the card
	def right_take_card(self):

        # store the Right Take of the arms
		Right_take = {'right_s0': -0.05675728915176031, 'right_s1':  -0.5284563814265251,
		'right_w0': -1.2824079386722058, 'right_w1': 0.7834806874124751, 'right_w2': 0.7447476725183684,
		'right_e0': 0.8490583660945765, 'right_e1': 2.0229371640238893}
		Left_idle = {'left_s0': 0.7040971816394049, 'left_s1': 0.8935438089432535,
		'left_w0': 0.8295001110490375, 'left_w1': 1.5665778796279788, 'left_w2': -0.11888351106111957,
		 'left_e0': -1.7452866414166295, 'left_e1': 1.6083788560978562}

        # move both arms to Right Take Position
		self.limb_left.move_to_joint_positions(Left_idle)
		self.limb_right.move_to_joint_positions(Right_take)

    # The position that the right arm deals the card to the baxter
	def right_deal_baxter(self):

        # store the Right arm deal to baxter position
		Right_self_deal = {'right_s0': 0.4586602555777387, 'right_s1':  0.42452918304728826,
		'right_w0': -2.0819954243574776, 'right_w1': 1.6559322605223041, 'right_w2': -0.14150972768242942,
		'right_e0': 1.6505633277647052, 'right_e1': 1.7337817855074888}
		Left_idle = {'left_s0': 0.7040971816394049, 'left_s1': 0.8935438089432535,
		'left_w0': 0.8295001110490375, 'left_w1': 1.5665778796279788, 'left_w2': -0.11888351106111957,
		 'left_e0': -1.7452866414166295, 'left_e1': 1.6083788560978562}

        # move right arm to preferred position
		self.limb_right.move_to_joint_positions(Right_self_deal)

    # The position that the right arm deals to player 1
	def right_deal_player1(self):

        # store the Right arm deal to Player 1 position
		position =  [0.0, 0.004601942363656241, -1.1343787926412634, 1.9378012302962488,
		-0.07861651537912745, -0.9644904203829539, 0.6715000898968398, 0.9878836273982065,
		-0.5019952128355016, 3.056840215058658, 0.3727573314561555, 0.6231796950784493,
		0.3593349995621582, -3.0441848735586037, 1.5002332105519347, -0.39078160571380915, -12.565987119160338]
		Left_idle = {'left_s0': -0.3608689803500436, 'left_s1': 0.22166022384944228,
		'left_w0': 2.918781944148971, 'left_w1': 1.4814419459003383, 'left_w2': -1.4338885414758904,
		'left_e0': -2.9019081554822312, 'left_e1': 0.2109223583342444}

        # Format the input message
		Right_1_deal = card_look_position = self.formatJoints(position,1)

		# Move the right arm to player 1
		self.limb_right.move_to_joint_positions(Right_1_deal)

    # The position that the right arm deals to player 2
	def right_deal_player2(self):

        # store the Right arm deal to Player 2 position
		position = [0.0, 0.004601942363656241, -1.1347622878382349,1.9381847254932203,
		-0.07746602978821339, -0.9648739155799253, 0.672650575487754,0.9878836273982065,
		-0.502378708032473, 3.055306234270773, 0.18254371375836423, 1.0799224746713312,
		0.26384469551629114, -3.006602344255411, 1.4158642672182369, 0.16605342028859604, -12.565987119160338]
		Left_idle = {'left_s0': -0.3608689803500436, 'left_s1': 0.22166022384944228,
		'left_w0': 2.918781944148971, 'left_w1': 1.4814419459003383, 'left_w2': -1.4338885414758904,
		'left_e0': -2.9019081554822312, 'left_e1': 0.2109223583342444}

		# Format the input message
		Right_2_deal = card_look_position = self.formatJoints(position,1)

		# Move the right arm to player 2
		self.limb_right.move_to_joint_positions(Right_2_deal)

    # The position that the right arm deals to player 3
	def right_deal_player3(self):

        # store the Right arm deal to Player 3 position
		position =  [0.0, 0.004218447166684887, -1.1228739367321228, 0.7374612637759127,
		0.28532042654668693, -0.6327670750027332, 0.8406214717612067, 1.868388599644434,
		-0.0007669903939427069, 3.0537722534828875, 0.31906800388016604, 1.513655542445932,
		0.2757330466224031, -3.0461023495434603, 1.5650438988400934, 0.8816554578371415, -12.565987119160338]
		Left_idle = {'left_s0': -0.3608689803500436, 'left_s1': 0.22166022384944228,
		'left_w0': 2.918781944148971, 'left_w1': 1.4814419459003383, 'left_w2': -1.4338885414758904,
		'left_e0': -2.9019081554822312, 'left_e1': 0.2109223583342444}

		# Format the input message
		Right_3_deal = card_look_position = self.formatJoints(position,1)

		# Move the right arm to player 3
		self.limb_right.move_to_joint_positions(Right_3_deal)

    # The home position
	def home_pose(self):

		# store the home position of the arms
		home_right = {'right_s0': 0.08, 'right_s1': -1.00, 'right_w0': -0.67,
		'right_w1': 1.03, 'right_w2': 0.50, 'right_e0': 1.18, 'right_e1': 1.94}
		home_left = {'left_s0': -0.08, 'left_s1': -1.00, 'left_w0': 0.67,
		'left_w1': 1.03, 'left_w2': -0.50, 'left_e0': -1.18, 'left_e1': 1.94}

		# move both arms to home position
		self.limb_right.move_to_joint_positions(home_right)
		self.limb_left.move_to_joint_positions(home_left)
		print("Returned to home position")

	#======================LEFT ARM==============================
	#move left arm out of the way
	def left_reset(self):
		joints = [TODO]

		self.setConstr([], 0)
		self.plan_and_executeFK(joints, 0)

    # Function returns the image in the left arm camera
	def Leftdecision(self):

        # Predefined location for the left arm
		position = [0.0, 0.0038349519697135344, -2.0666556164786236, -0.049854375606275945, -0.5921165841237697, -0.3470631532590749, 2.0125827937056626, 1.545102148597583, 0.1392087565006013, -0.06941263065181497, 0.279951493789088, 0.813776807973212, -0.5135000687446423, -3.0487868159222598, -1.570029336400721, 1.2896943474146616, -12.565987119160338]

        # Format the input message
		over_card_position = self.formatJoints(position,0)

        # Move the Left limb ad return the transformed image
		self.limb_left.move_to_joint_positions(over_card_position)
		image = self.Left_read_image()
		return image

	#======================BOTH ARMS==============================

    # The position that uses the right hand camera to identify the poker
    # Return the read image
	def RightHandLook(self):

        # The message that stores the recorded robot states
		position = [0.0, 0.004218447166684887, -1.7429856702348014, 1.6099128368857418, 0.7014127152606054, 0.8920098281553681,
		0.8283496254581234, 1.564660403643122, -0.12041749184900498, 1.4058933920969816, 2.0198692024481186, -0.38234471138043935,
		-0.2577087723647495, -1.4381069886425755, 1.3169225063996277, 0.7351602925940846, -12.565987119160338]

        # Format the input message
		card_look_position = self.formatJoints(position,1)

		# move the right arm to the right look position and return the read image
		self.limb_right.move_to_joint_positions(card_look_position)
		image = self.Right_read_image()
		return image

    # The position having some distance with the first card in the card box
	def RightOverCard(self):

        #The message that stores the recorded robot states
		position = [0.0, 0.003451456772742181, -1.1807817114747972, 1.936650744705335, -0.08245146734884098, -0.9970875121255189, 0.6722670802907825, 1.031985575049912, -0.501228222441559, 1.7065536265225227, 2.25265078700973, -0.4939418136991032, 0.6285486278360483, -2.0931167850696473, 2.0037624041753217, -0.9326603190343316, -12.565987119160338]

        # Format the input message
        over_card_position = self.formatJoints(position,1)

		# move the right arm to safe distance
		self.limb_right.move_to_joint_positions(over_card_position)

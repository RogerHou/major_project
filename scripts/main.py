#!/usr/bin/env python
# coding: utf-8
from __future__ import print_function
from __future__ import division

# import libraies
import time
import turtle
import speech_recognition as sr
import numpy as np
import math

# import ros related
import sys
import rospy
import os
import cv2
import cv_bridge

# import local libraies
from card import Card
from deck import Deck
from table import Table
from player import Player
from dealer import Dealer
from commandwn import Commandwn
from speech import recognize_speech_from_mic

# import ros related local libraries 
from arm import ik_test,ArmPlanner
from baxter_interface import Limb
from controller import Controller
from baxter_interface import gripper as robot_gripper
from sensor_msgs.msg import (Image,Range)

###################
def IR_callback(msg):
    global distance
    distance = msg
    #print(msg.range)

def display_image(path, sleepTime = 0):

    img = cv2.imread(path)
    msg = cv_bridge.CvBridge().cv2_to_imgmsg(img, encoding="bgr8")
    img_pub = rospy.Publisher('/robot/xdisplay', Image, latch=True, queue_size=1)
    img_pub.publish(msg)

##################

def game(dealer):
    names = []

    # ask for bet placing
    dealer.place_bet()

    # get the name of players
    for i in range(dealer.player_num):
        names.append(dealer.players[i].name)

    dealer.deck.reset_deck()

    #for card in dealer.deck.cards:
    #    print card.rank,card.suit

    dealer.table.frame(names)

    # show bet
    for i in range(dealer.player_num):
        dealer.table.show_bet(dealer.players[i].x,dealer.players[i].y,dealer.players[i].bet,dealer.players[i].bet_pen)

    # start the game by dealling cards
    for i in range(dealer.player_num):
        dealer.player_draw(i)

    # one card for the dealer
    dealer.banker_draw()

    # second run for the players
    for i in range(dealer.player_num):
        dealer.player_draw(i)

    # ask the player
    for i in range(dealer.player_num):
        # reset the flag for the next player
        dealer.wn.select_flag = 1

        # check if start black jack
        if dealer.players[i].point == 21:
            print("Player "+dealer.players[i].name+" receives blackjack")
        else:
            while True:
                print("Player "+dealer.players[i].name+" please select your response: ")
                choice = dealer.wn.menu(dealer.poker_classifier,dealer.robot)

                if choice == 0:
                    print("PASS!")
                    break
                elif choice == 1:
                    print("HIT!")
                    dealer.player_draw(i)
                    # update the flag
                    dealer.wn.select_flag = 2
                elif (choice == 2 and dealer.wn.select_flag != 2):
                    print("DOUBLE!")
                    dealer.players[i].bet = dealer.players[i].bet*2
                    dealer.players[i].bet_pen.clear()
                    dealer.table.show_bet(dealer.players[i].x,dealer.players[i].y,dealer.players[i].bet,dealer.players[i].bet_pen)
                    dealer.player_draw(i)
                    break

                if dealer.players[i].point > 21:
                    print("Player "+dealer.players[i].name+" bust")
                    dealer.players[i].point = 0
                    break
                elif dealer.players[i].point == 21:
                    print("Player "+dealer.players[i].name+" receives blackjack")
                    break


    # banker second card
    dealer.banker_draw()

    # add delay
    time.sleep(0.5)

    # choose hit or stand
    while dealer.decision() == 1:
        # show the choice
        dealer.penx.penup()
        if dealer.player_num == 3:
            dealer.penx.goto(-80,250)
        else:
            dealer.penx.goto(320,250)

        dealer.penx.pendown()
        dealer.penx.color("#FF796C")
        dealer.penx.write("Hit",False,font=("Courier New", 25, "bold"))

        # draw new card
        dealer.banker_draw()

        # clear the note
        dealer.penx.clear()

        if dealer.point == 0:
            break

        # add delay
        time.sleep(0.5)
    # show results
    dealer.judge()

def main():
    # robot setup
    rospy.init_node('moveit_node')

    # createa sceen to simulate the game
    # 1. main screen of the game
    wn = turtle.Screen()
    wn.bgcolor("#008000")
    table_w = 600
    table_h = 800
    wn.setup(table_h,table_w)
    wn.title("BJ simulator")

    # 2. initialize an instuction window
    commandwn = Commandwn()
    txt = ["Game","Loading..."]
    commandwn.message_display(txt)

    # create recognizer and mic instances
    recognizer = sr.Recognizer()
    microphone = sr.Microphone()

    # define how many player in the game
    print("PLease specify how many players in the game: (say 1/2/3)")
    while True:
        for j in range(10):
            msg = recognize_speech_from_mic(recognizer, microphone)
            if msg["transcription"]:
                break
            if not msg["success"]:
                break
            print("I didn't catch that. What did you say?\n")

        # if there was an error, stop the game
        if msg["error"]:
            print("ERROR: {}".format(msg["error"]))
            break

        # show the user the transcription
        print("You said: {}".format(msg["transcription"]))

        # check any digit in the senstence
        digits = []
        for i in msg["transcription"]:
            if i.isdigit():
                digits.append(i)

        if len(digits) == 1:
            player_num = int(digits[0])
            break


    # initialize the robot
    dealer = Dealer(player_num,table_w,table_h,commandwn)

    # main loop
    while True:
        # run the game
        dealer.robot.home_pose()
        game(dealer)

        # after onece ask the user whether play again
        again_flag = raw_input("Would you wanna to play again? (y/n): ")
        while True:
            if (again_flag == 'y' or again_flag == 'Y'):
                break
            elif (again_flag == 'n' or again_flag == 'N'):
                print("See ya next time")
                exit()
            else:
                again_flag = raw_input("Undefined response, please select y/n: ")

        # clear all drawing and cache
        dealer.refresh()
        for i in range(dealer.player_num):
            dealer.players[i].refresh()

    turtle.mainloop()

if __name__ =="__main__":
    main()
    pygame.quit()

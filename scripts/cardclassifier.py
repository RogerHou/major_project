# coding: utf-8
import cv2
import numpy as np
import math



import os
import re
import collections
class Cardclassifier(object):
    def __init__(self):
        self.rank_template = collections.defaultdict(list)
        self.suit_template = collections.defaultdict(list)
        self.img_size = (480,660)
        self.rank_size =(65,111)
        self.suit_size =(60,60)
        self.load_template('/home/roger/sourcePoker/card_template/')


    def load_template(self,path):
        #load rank template
        for fname in os.listdir(path+'rank/'):
            card_name = fname.split('.')[0]

            card_img = cv2.imread(path+'rank/' + fname)
            card_img = cv2.resize(card_img, self.rank_size)
            card_img = cv2.cvtColor(card_img, cv2.COLOR_BGR2GRAY)
            flag, card_img = cv2.threshold(card_img, 160, 255, cv2.THRESH_BINARY)

            self.rank_template[card_name] = card_img
        #load suit template
        for fname in os.listdir(path+'suit/'):
            card_name = fname.split('.')[0]

            card_img = cv2.imread(path+'suit/' + fname)
            card_img = cv2.resize(card_img, self.suit_size)
            card_img = cv2.cvtColor(card_img, cv2.COLOR_BGR2GRAY)
            flag, card_img = cv2.threshold(card_img, 160, 255, cv2.THRESH_BINARY)

            self.suit_template[card_name] = card_img
    def card_classify(self,rank,suit):
        #check rank
        diff_min = np.inf
        match_ress=None
        label_rank = None
        for rank_name,rank_img in self.rank_template.items():
            cur_diff = cv2.matchTemplate(rank, rank_img, cv2.TM_SQDIFF_NORMED)

            if cur_diff <diff_min:

                diff_min=cur_diff
                label_rank = rank_name
        #check suit
        diff_min = np.inf
        match_ress=None
        label_suit= None
        for suit_name,suit_img in self.suit_template.items():
            cur_diff = cv2.matchTemplate(suit, suit_img, cv2.TM_SQDIFF_NORMED)

            if cur_diff <diff_min:
                diff_min=cur_diff
                label_suit = suit_name

        return label_rank,label_suit


    @classmethod
    def pdist(cls,p1,p2):
        return math.sqrt(math.pow((p2[0] - p1[0]), 2) + math.pow((p2[1] - p1[1]), 2))

    #detect and crop the card from the image
    def detect_card(self,img,number = 1,i=1):

        #convert to HSV
        img_hsv = cv2.cvtColor(img,cv2.COLOR_BGR2HSV)
        #apply gaussion filter
        img_hsv = cv2.GaussianBlur(img_hsv, (5,5), 2)
        #use color mask to detect white parts
        threshold_low = np.array([0, 0, 220])
        threshold_high = np.array([180, 30, 255])
        white_mask = cv2.inRange(img_hsv, threshold_low, threshold_high)
        # #find contours
        binary,contours, hierarchy = cv2.findContours(white_mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        contour = sorted(contours, key=cv2.contourArea, reverse=True)[:number]
        cv2.drawContours(img, contour, -1, (0, 255, 0), 3)

        #find rect bounding box
        bounding = cv2.boundingRect(contour[0])
        [x, y, w, h] = bounding

        #clop the image based on bounding
        clip_img= img[y:y + h, x:x + w]
        #check card direction and decide whether to rotate 90 degree
        (h,w,_) = clip_img.shape
        if h < w:
            clip_img = cv2.transpose(clip_img)
            clip_img = cv2.flip(clip_img, 1)
        #resize the image
        clip_img = clip_img[10:80, 5:40]
        kernel = np.array([[0, -1, 0], [-1, 5, -1], [0, -1, 0]], np.float32)  #
        clip_img = cv2.filter2D(clip_img, -1, kernel=kernel)


        #detect the suit and rank
        rank_img, suit_img = self.detect_label(clip_img)
        rank,suit=self.card_classify( rank_img,suit_img)
        return rank,suit






    def detect_label(self,card_img):
        # transfer to gray
        img_gray = cv2.cvtColor(card_img, cv2.COLOR_BGR2GRAY)

        # thresholding the image
        flag, thresh = cv2.threshold(img_gray, 160, 255, cv2.THRESH_BINARY)
        thresh = np.invert(thresh)
        # find contours
        binary,contours, hierarchy = cv2.findContours(thresh, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

        contours = sorted(contours, key=cv2.contourArea, reverse=True)[:2]
        cv2.drawContours(card_img , contours, -1, (0, 255, 0), 3)


        crop_imgs = []
        Areas = []
        for i in range(2):
            temp_bounding = cv2.boundingRect(contours[i])
            [x, y, w, h] = temp_bounding
            crop_imgs.append(thresh[y:y + h, x:x + w])
            Areas.append(w*h)
        if Areas[0] > Areas[1]:
            crop_rank = cv2.resize(crop_imgs[0],self.rank_size)
            crop_suit = cv2.resize(crop_imgs[1], self.suit_size)
        else:
            crop_rank = cv2.resize(crop_imgs[1],self.rank_size)
            crop_suit = cv2.resize(crop_imgs[0], self.suit_size)


        return crop_rank,crop_suit
    @classmethod
    def action_classify(cls,img,threshold =0.15):
        labels = ['red','green','yellow','None']
        current_maxpositive = 0

        #convert of hsv

        img_hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)

        #define high and low threshold for mask
        sign_num = 3
        threshold_low = np.array([[0,0,0.3*255],[0.15*255,0,0.3*255],[0.069*255,0,0.3*255]])
        threshold_high = np.array([[0.043*255,255,255],[0.350*255,255,255],[0.134*255,255,255]])
        (m,n,_)= img.shape
        threshold = threshold*m*n
        current_label = labels[3]
        for idx in range(sign_num):

            cur_mask = cv2.inRange(img_hsv, threshold_low[idx, :], threshold_high[idx, :])

            positive_pnum =len(list(np.where(cur_mask==255)[0]))

            if positive_pnum >threshold and positive_pnum >current_maxpositive:
                 current_label = labels[idx]
                 current_maxpositive=positive_pnum
        return current_label

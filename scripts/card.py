from __future__ import print_function
from __future__ import division

# define symbols
symbols = {"spades":u"\u2660","hearts":u"\u2665","clubs":u"\u2663","diamonds":u"\u2666"}
values = {"A":1,"K":10,"Q":10,"J":10,"10":10,"9":9,"8":8,"7":7,"6":6,"5":5,"4":4,"3":3,"2":2}

# create classes
class Card():
    def __init__(self, rank, suit):
        self.rank = rank
        self.suit = suit
        self.value = values[self.rank]

    def print_card(self):
        print(self.rank, end="")
        print(symbols[self.suit].encode("utf-8"))

    def render(self, x, y, pen):
        pen.speed(0)
        pen.hideturtle()

        if (self.suit == "spades" or self.suit == "clubs"):
            color_fill = "black"
        else:
            color_fill = "#E50000"

        pen.penup()
        pen.goto(x,y)
        pen.color("white")
        pen.goto(x-50,y+75)
        pen.begin_fill()
        pen.pendown()
        pen.goto(x+50,y+75)
        pen.goto(x+50,y-75)
        pen.goto(x-50,y-75)
        pen.goto(x-50,y+75)
        pen.end_fill()
        pen.penup()

        if self.rank != "":
            # Draw suit in the middle
            pen.color(color_fill)
            pen.goto(x-12,y-25)
            pen.write(symbols[self.suit].encode("utf-8"),False,font=("Courier New", 32, "normal"))


            if self.rank == "10":
                # Draw top left
                pen.goto(x-47,y+45)
                pen.write(self.rank,False,font=("Courier New", 18, "normal"))
                pen.goto(x-40,y+25)
                pen.write(symbols[self.suit].encode("utf-8"),False,font=("Courier New", 18, "normal"))

                # Draw bottom right
                pen.goto(x+23,y-60)
                pen.write(self.rank,False,font=("Courier New", 18, "normal"))
                pen.goto(x+30,y-80)
                pen.write(symbols[self.suit].encode("utf-8"),False,font=("Courier New", 18, "normal"))

            else:
                # Draw top left
                pen.goto(x-40,y+45)
                pen.write(self.rank,False,font=("Courier New", 18, "normal"))
                pen.goto(x-40,y+25)
                pen.write(symbols[self.suit].encode("utf-8"),False,font=("Courier New", 18, "normal"))

                # Draw bottom right
                pen.goto(x+30,y-60)
                pen.write(self.rank,False,font=("Courier New", 18, "normal"))
                pen.goto(x+30,y-80)
                pen.write(symbols[self.suit].encode("utf-8"),False,font=("Courier New", 18, "normal"))

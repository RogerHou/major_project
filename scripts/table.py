import turtle

class Table():
    def __init__(self, player_num,table_w,table_h):
        self.screen = player_num + 1
        self.pen = turtle.Turtle()
        self.pen.speed(0)
        self.pen.hideturtle()
        self.table_w = table_w
        self.table_h = table_h

    def frame(self,names):
        # create a seperate line for the table
        self.pen.penup()
        self.pen.pensize(6)
        self.pen.goto(-self.table_h/2+5,-25)
        self.pen.color("white")
        self.pen.pendown()
        #self.pen.forward(self.table_h)
        self.pen.goto(self.table_h/2-9,-25)
        self.pen.goto(self.table_h/2-9,25)
        self.pen.goto(-self.table_h/2+5,25)
        self.pen.goto(-self.table_h/2+5,-25)
        self.pen.penup()

        self.pen.color("#FFFF14")
        self.pen.goto(-85,-25)
        self.pen.write("Casino",False,font=("Courier New", 32, "normal"))

        if self.screen == 2:
            # draw the name of the banker
            self.pen.color("#DC143C")
            self.pen.goto(-55,270)
            self.pen.write("Banker",False,font=("Courier New", 20, "bold"))

            # draw the player's name
            self.pen.color("#FFD700")
            self.pen.goto(-10-len(names[0])*7.5,-290)
            self.pen.write(names[0],False,font=("Courier New", 20, "normal"))
        elif self.screen == 3:
            # draw a seperate line
            self.pen.penup()
            self.pen.pensize(6)
            self.pen.goto(0,-25)
            self.pen.color("white")
            self.pen.pendown()
            self.pen.goto(0,-self.table_w/2)
            self.pen.penup()

            # draw the name of the banker
            self.pen.color("#DC143C")
            self.pen.goto(-55,270)
            self.pen.write("Banker",False,font=("Courier New", 20, "bold"))

            # draw the player's name
            self.pen.color("#FFD700")
            self.pen.goto(-210-len(names[0])*7.5,-60)
            self.pen.write(names[0],False,font=("Courier New", 20, "normal"))

            self.pen.color("#FFD700")
            self.pen.goto(190-len(names[1])*7.5,-60)
            self.pen.write(names[1],False,font=("Courier New", 20, "normal"))

        elif self.screen == 4:
            # draw a seperate line
            self.pen.penup()
            self.pen.pensize(6)
            self.pen.goto(0,-25)
            self.pen.color("white")
            self.pen.pendown()
            self.pen.goto(0,-self.table_w/2)
            self.pen.penup()
            # draw a seperate line
            self.pen.penup()
            self.pen.pensize(6)
            self.pen.goto(0,25)
            self.pen.color("white")
            self.pen.pendown()
            self.pen.goto(0,self.table_w/2)
            self.pen.penup()

            # draw the name of the banker
            self.pen.color("#DC143C")
            self.pen.goto(-255,270)
            self.pen.write("Banker",False,font=("Courier New", 20, "bold"))

            # draw the player's name
            self.pen.color("#FFD700")
            self.pen.goto(-210-len(names[1])*7.5,-60)
            self.pen.write(names[1],False,font=("Courier New", 20, "normal"))

            self.pen.color("#FFD700")
            self.pen.goto(190-len(names[2])*7.5,-60)
            self.pen.write(names[2],False,font=("Courier New", 20, "normal"))

            self.pen.color("#FFD700")
            self.pen.goto(190-len(names[0])*7.5,270)
            self.pen.write(names[0],False,font=("Courier New", 20, "normal"))
    def draw_cards(self,x,y,cards,ID,points,pen):
        # clear previous draw first
        pen.clear()

        # define player area and banker area
        if self.screen == 2:
            banker_area = self.table_h
            player_area = self.table_h
        elif self.screen == 3:
            banker_area = self.table_h
            player_area = self.table_h/2
        elif self.screen == 4:
            banker_area = self.table_h/2
            player_area = self.table_h/2

        # add an iterator
        i = 0;

        # delta x and start x calculation
        if ID == 0:
            if (self.screen == 2 or self.screen == 3):
                if len(cards) < 6:
                    start_x = -60*len(cards) + 50 + x
                    dx = 120
                else:
                    start_x = -banker_area/2 + 70 + x
                    dx =  round((banker_area - 80)/len(cards))

            else:
                if len(cards) < 4:
                    start_x = -60*len(cards) + 50 + x
                    dx = 120
                else:
                    start_x = -banker_area/2 + 70 + x
                    dx =  round((banker_area - 80)/len(cards))

            # show the statistic
            pen.penup()
            pen.goto(-380,30)
            pen.pendown()
            pen.color("#FFA550")
            if (points == 0 or points > 21):
                pen.write("Point: bust",False,font=("Courier New", 18, "normal"))
            else:
                pen.write("Point:"+str(points),False,font=("Courier New", 18, "normal"))

        else:
            pen.penup()
            if self.screen == 2:
                if len(cards) < 5:
                    start_x = -60*len(cards) + 50 + x
                    dx = 120
                else:
                    start_x = -player_area/2 + 70 + x
                    dx =  round((player_area - 80)/len(cards))

                pen.goto(-380,-270)
            else:
                if len(cards) < 4:
                    start_x = -60*len(cards) + 50 + x
                    dx = 120
                else:
                    start_x = -player_area/2 + 70 + x
                    dx =  round((player_area - 80)/len(cards))
                pen.goto(x-player_area/2+20, y-120)

            # show the statistic
            pen.pendown()
            pen.color("#FFA550")
            if (points == 0 or points > 21):
                pen.write("Point: bust",False,font=("Courier New", 18, "normal"))
            elif points == 21:
                pen.write("Point: blackjack",False,font=("Courier New", 18, "normal"))
            else:
                pen.write("Point:"+str(points),False,font=("Courier New", 18, "normal"))

        # write cards
        for card in cards:
            card.render(start_x+i*dx,y,pen)
            i = i+1

    def show_bet(self,x,y,bet,pen):
        pen.penup()
        if bet < 0:
            x = x - 15
        if abs(bet) < 10:
            if self.screen == 2:
                pen.goto(x+self.table_h/2-70,int(y>0)*20+y+self.table_w/4-61)
            elif y >= 0:
                pen.goto(x+self.table_h/4-70, y+110)
            else:
                pen.goto(x+self.table_h/4-70, y+80)
        elif abs(bet) < 100:
            if self.screen == 2:
                pen.goto(x+self.table_h/2-84,int(y>0)*20+y+self.table_w/4-61)
            elif y >= 0:
                pen.goto(x+self.table_h/4-84, y+110)
            else:
                pen.goto(x+self.table_h/4-84, y+80)

        # show the bet
        pen.pendown()
        if bet < 0:
            pen.color("#15B01A")
        else:
            pen.color("#FF796C")
        pen.write(str(bet)+"x".encode("utf-8"),False,font=("Courier New", 18, "normal"))

        pen.penup()
        if bet < 0:
            x = x + 15

        if self.screen == 2:
            pen.goto(x+self.table_h/2-40,int(y>0)*20+y+self.table_w/4-70)
        elif y >= 0:
            pen.goto(x+self.table_h/4-40, y+100)
        else:
            pen.goto(x+self.table_h/4-40, y+70)

        # show the bet
        pen.pendown()
        pen.write(u"\u235F".encode("utf-8"),False,font=("Courier New", 28, "normal"))

    def result(self,x,y,player_p,banker_p,pen):
        pen.penup()
        if self.screen == 2:
            pen.goto(330,-270)
        else:
            pen.goto(x+self.table_h/4-70, y-120)

        # show the statistic
        pen.pendown()
        pen.color("#FF796C")
        if (player_p > banker_p and player_p > 0 and player_p < 22) or (player_p == 21):
            pen.write("Win",False,font=("Courier New", 18, "normal"))
        elif (player_p == banker_p and player_p !=0):
            pen.write("Draw",False,font=("Courier New", 18, "normal"))
        else:
            pen.write("Loss",False,font=("Courier New", 18, "normal"))
